﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SampleApp1
{
    public partial class Form2 : Form
    {
        Task execution;
        CancellationTokenSource tokenSource = new CancellationTokenSource();

        private Action<int> setProgress;

        public Form2()
        {
            InitializeComponent();

            setProgress = new Action<int>(SetProgress);
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            execution = Task.Run(async () =>
            {
                foreach (var status in Enumerable.Range(1, 10))
                {
                    if (tokenSource.Token.IsCancellationRequested)
                    {
                        MessageBox.Show("キャンセルされました。");
                        return;
                    }

                    await Task.Delay(1000);
                    this.Invoke(setProgress, 10);
                }
            }, tokenSource.Token);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tokenSource.Cancel();
        }

        private void SetProgress(int value)
        {
            progressBar1.Value += value;
        }
    }
}
